import org.joda.time.DateTime;
import server.ServerA;
import simmulation.Context;
import simmulation.Event;
import simmulation.ServerAEvents.EventCreateMessage;
import simmulation.Heap;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;


/**
 * Created by jorge on 6/9/15.
 */
public class Sim {

    public static void main(String[] args)
    {
        int simulationsQty = 1;
        int maxTime = 3600;
        int timerTime = 7;
        boolean defaultMode = false;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter simulations quantity:");
            String strSimQty = br.readLine();

            System.out.print("Per simulation max time(in seconds):");
            String strMaxTime = br.readLine();

            System.out.print("Server A timer time:");
            String strTimerTime = br.readLine();

            System.out.print("Slow mode (y/N):");
            String strDefaultMode = br.readLine();

            try {
                simulationsQty = Integer.parseInt(strSimQty);
                maxTime = Integer.parseInt(strMaxTime);
                timerTime = Integer.parseInt(strTimerTime);
                defaultMode = strDefaultMode.equals("y");

            } catch (Exception nfe) {
                System.err.println("Invalid Format!");
            }
        }catch (Exception e)
        {
            System.err.println(e);
        }


        for(int i = 0; i < simulationsQty; ++i) {

            Heap eventHeap = new Heap<>(2500);
            Context context = new Context(eventHeap);
            context.getServerA().setTimerSeconds(timerTime);
            Event firstEvent = new EventCreateMessage(context);
            DateTime actualDateTime = new DateTime();
            DateTime stopDateTime = actualDateTime.plusSeconds(maxTime);
            firstEvent.setExecutionTime(actualDateTime);
            eventHeap.insert(firstEvent);
            ;
            while (eventHeap.size() > 0 && actualDateTime.compareTo(stopDateTime) < 0 ) {
                Event actualEvent = (Event) eventHeap.min();
                eventHeap.removeMin();//remove actual event
                context.setActualTime(actualEvent.getExecutionTime());
                actualEvent.execute();

                ArrayList<Event> nextEvents = actualEvent.generateNextEvents();

                for (int y = 0; y < nextEvents.size(); ++y) {
                    eventHeap.insert(nextEvents.get(y));
                }

                ServerA serverA = context.getServerA();
                int messageQueueSize = context.getServerA().messagesQueueSize();
                long lastACKReceived = serverA.getLastAckReceived() != null ? serverA.getLastAckReceived().getFrameId() : -1;
                String listQueue = serverA.getMessagesList().printList(20);

                context.printLn("Run: " +  actualEvent);
                context.printLn("\tServer A messages queue size:    " + messageQueueSize);
                context.printLn(listQueue);
                context.printLn("\tServer A last ack received: " + lastACKReceived);

                if (defaultMode) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                actualDateTime = context.getActualTime();
            }

            context.printLn("--------------SIMULATION " + i + "END----------------------");

        }

    }
}
