package server;

import communication.Frame;
import simmulation.Event;

import java.util.*;

/**
 * Created by jorge on 6/9/15.
 */
public class ServerB extends Server {

    private Queue<Frame> receivedFrames;
    private long frameIdWaitingFor = 0;

    private boolean checkFramesProcessBusy;

    public ServerB()
    {
        super();
        receivedFrames = new LinkedList<>();
        checkFramesProcessBusy = false;
    }

    public void addReceivedFrame(Frame frame)
    {
        this.receivedFrames.add(frame);
    }


    /**
     * Get the first frame in the queue to check it
     * and remove it for the queue
     * @return
     */
    public Frame obtainFrameToCheck()
    {
        return this.receivedFrames.poll();
    }

    public boolean isCheckFramesProcessBusy() {
        return checkFramesProcessBusy;
    }

    public void setCheckFramesProcessBusy(boolean checkFramesProcessBusy) {
        this.checkFramesProcessBusy = checkFramesProcessBusy;
    }

    public void incrementFrameWaitId()
    {
        ++this.frameIdWaitingFor;
    }

    public long getFrameIdWaitingFor()
    {
        return frameIdWaitingFor;
    }

}
