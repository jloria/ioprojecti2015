package server;

import communication.Acknowledge;
import communication.ListMessages;
import communication.ListMessagesField;
import communication.Message;


import java.util.ArrayList;

/**
 * Created by jorge on 6/9/15.
 */
public class ServerA extends Server {

    //messages list
    private ListMessages messagesList;

    //already sent message list
    private ArrayList<ListMessagesField> sentMessages;

    //Id of the last sent frame
    private long lastSentFrameId = -1;

    //Id of next frame to send
    private long lastFrameId;

    //Send messages process is busy
    private boolean sendProcessBusy;

    //last ACK received
    private Acknowledge lastAckReceived;

    //the timer duration in seconds
    private long timerSeconds;

    public ServerA()
    {
        super();
        this.messagesList = new ListMessages();
        this.lastFrameId = -1;
        this.sendProcessBusy = false;
    }

    /**
     * Insert a new message to messages list
     * @param message
     */
    public void addMessageToList(Message message)
    {
        this.messagesList.addMessage(message);
    }

    public ListMessages getMessagesList() {
        return messagesList;
    }

    /**
     * Increments the id and return the new id to use
     * @return
     */
    public long incrementLastFrameId()
    {
        return ++this.lastFrameId;
    }

    public long getLastFrameId()
    {
        return this.lastFrameId;
    }

    public boolean isSendProcessBusy() {
        return sendProcessBusy;
    }

    public void setSendProcessBusy(boolean sendProcessBusy) {
        this.sendProcessBusy = sendProcessBusy;
    }

    public boolean canSendMoreMessageFromWindow()
    {
        return this.messagesList.windowHasMessageToSend();
    }

    public void shiftWindow(Acknowledge receivedAcknowledge){
        this.messagesList.shiftWindowToFrameId(receivedAcknowledge.getFrameId());
    }

    public int messagesQueueSize()
    {
        return this.messagesList.size();
    }

    public Acknowledge getLastAckReceived() {
        return lastAckReceived;
    }

    public void setLastAckReceived(Acknowledge lastAckReceived) {
        this.lastAckReceived = lastAckReceived;
    }

    public long getTimerSeconds()
    {
        return this.timerSeconds;
    }

    public void setTimerSeconds(long timerSeconds) {
        this.timerSeconds = timerSeconds;
    }

    /**
     * Tell to window shift window to resend
     */
    public void prepareWindowToResend()
    {
        this.messagesList.shiftWindowToResend();
    }

    public long getLastSentFrameId() {
        return lastSentFrameId;
    }

    public void setLastSentFrameId(long lastSentFrameId) {
        this.lastSentFrameId = lastSentFrameId;
    }
}
