package simmulation.ServerAEvents;

import communication.Frame;
import server.ServerA;
import simmulation.Context;
import simmulation.Event;

import java.util.ArrayList;

/**
 * Created by jorge on 6/19/15.
 */
public class EventTimer extends Event {

    private Frame frameToResend;

    private boolean programResend = false;

    public EventTimer(Context context, Frame frame)
    {
        super(context);
        frameToResend = frame;
    }

    @Override
    public void execute() {
        super.execute();
        if( context.getServerA().getLastAckReceived() != null ) {
            if (frameToResend.getId() > context.getServerA().getLastAckReceived().getFrameId()) {
                context.deleteAllSendMessageEvents();
                programResend = true;
            }
        }


    }

    @Override
    public ArrayList<Event> generateNextEvents() {
        ArrayList<Event> nextEvents = super.generateNextEvents();
        ServerA serverA = context.getServerA();
        if(programResend && !serverA.isSendProcessBusy() && serverA.canSendMoreMessageFromWindow() )
        {
            EventSendMessage sendMessage = new EventSendMessage(context);
            sendMessage.setExecutionTime(context.getActualTime());
            nextEvents.add(sendMessage);
        }

        return nextEvents;
    }

    public Frame getFrameToResend()
    {
        return this.frameToResend;
    }

}
