package simmulation.ServerAEvents;

import communication.ListMessagesField;
import org.joda.time.DateTime;
import simmulation.Context;
import simmulation.Event;
import simmulation.ServerBEvents.EventReceiveFrame;

import java.util.ArrayList;

/**
 * Created by jorge on 6/12/15.
 *
 * Class for execute event of message already sent
 */
public class EventFrameSent extends Event {

    private ListMessagesField messagesField;

    public EventFrameSent(Context context, ListMessagesField messagesField)
    {
        super(context);
        this.messagesField = messagesField;
    }

    @Override
    public void execute() {
        super.execute();
        if(messagesField != null) {
            this.messagesField.setSent(true);
            this.messagesField.getFrame().setSent(true);
            this.messagesField.getFrame().setHasError(context.willTheFrameHaveErrors());
            //free the send messages process
            this.context.getServerA().setSendProcessBusy(false);
            this.context.getServerA().setLastSentFrameId(messagesField.getFrame().getId());
        }
    }

    @Override
    public ArrayList<Event> generateNextEvents() {

        ArrayList<Event> nextEvents = new ArrayList<>();

        //generate events for:
        // server A, send next message available in list
        // if window is not sent
        if(context.getServerA().canSendMoreMessageFromWindow()) {
            EventSendMessage sendNextMessageEvent = new EventSendMessage(context);
            sendNextMessageEvent.setExecutionTime(context.getActualTime());
            nextEvents.add(sendNextMessageEvent);
        }

        //server A, timer for this frame
        EventTimer eventTimer = new EventTimer(context,messagesField.getFrame());
        long millisToEventTimer = (long) context.convertSecsToMillis(context.getServerA().getTimerSeconds());
        DateTime eventTimerDate = context.getActualTime().plus(millisToEventTimer);
        eventTimer.setExecutionTime(eventTimerDate);
        nextEvents.add(eventTimer);


        // server B, receiving the message
        if( !context.willTheFrameBeLost() )
        {
            EventReceiveFrame eventReceiveFrame = new EventReceiveFrame(context, messagesField.getFrame());
            long secsToReceiveFrame = context.generateReceiveFrameTime();
            long millisToReceiveFrame = (long)context.convertSecsToMillis(secsToReceiveFrame);
            DateTime eventExecutionTime = context.getActualTime().plus(millisToReceiveFrame);
            eventReceiveFrame.setExecutionTime(eventExecutionTime);
            nextEvents.add(eventReceiveFrame);
        }

        return nextEvents;
    }
}
