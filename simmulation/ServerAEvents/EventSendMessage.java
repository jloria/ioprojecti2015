package simmulation.ServerAEvents;

import communication.Frame;
import communication.ListMessagesField;
import org.joda.time.DateTime;
import simmulation.Context;
import simmulation.Event;

import java.util.ArrayList;

/**
 * Created by jorge on 6/11/15.
 */
public class EventSendMessage extends Event {

    private ListMessagesField messageField;
    private boolean thereIsAMessageToSend  = false;

    public EventSendMessage(Context context){super(context);}

    @Override
    public void execute() {
        super.execute();

        messageField = this.context.getServerA().getMessagesList().getNextMessageToSend();
        thereIsAMessageToSend = messageField != null;
        if(thereIsAMessageToSend)
        {
            if( messageField.getFrame() == null )
            {
                //crear un frame
                long frameId = this.context.getServerA().incrementLastFrameId();
                Frame frame = new Frame( frameId, messageField.getMessage() );
                messageField.setFrame(frame);
            }else{//already exists a frame
                messageField.setSent(false);
            }
            //set server send message process state, to sending
            this.context.getServerA().setSendProcessBusy(true);
            thereIsAMessageToSend = true;
        }

    }

    @Override
    public ArrayList<Event> generateNextEvents() {
        ArrayList<Event> nextEvents = super.generateNextEvents();

        if(thereIsAMessageToSend) {
            //already sent message event
            double frameSentSecs = this.context.generateSentFrameTime();
            long frameSentMillis = (long) this.context.convertSecsToMillis(frameSentSecs);
            DateTime executionSent = this.context.getActualTime().plus(frameSentMillis);
            EventFrameSent eventFrameSent = new EventFrameSent(this.context, this.messageField);
            eventFrameSent.setExecutionTime(executionSent);
            nextEvents.add(eventFrameSent);
        }

        return nextEvents;
    }
}
