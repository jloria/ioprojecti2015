package simmulation.ServerAEvents;

import communication.Message;
import org.joda.time.DateTime;
import server.ServerA;
import simmulation.Context;
import simmulation.Event;

import java.util.ArrayList;

/**
 * Event for creating messages and pushing it to ServerA list of
 * pending messages
 */
public class EventCreateMessage extends Event {

    public EventCreateMessage(Context context) {
        super(context);
    }

    @Override
    public void execute() {
        super.execute();
        Message message = new Message();
        this.context.getServerA().addMessageToList(message);
    }

    @Override
    public ArrayList<Event> generateNextEvents() {
        ArrayList<Event> nextEvents = new ArrayList<>();


        //Next message creation event
        EventCreateMessage createMessageEvent = new EventCreateMessage(this.context);
        DateTime actualTime = context.getActualTime();
        double secsFuture = context.generateCreateMessageTime();
        double millisFuture = context.convertSecsToMillis(secsFuture);
        DateTime nextEventDateTime = actualTime.plus( (long)millisFuture );
        createMessageEvent.setExecutionTime(nextEventDateTime);
        nextEvents.add(createMessageEvent);

        //check if process in server A is not busy
        ServerA serverA = context.getServerA();
        if(!serverA.isSendProcessBusy() && serverA.canSendMoreMessageFromWindow() )
        {
            EventSendMessage eventSendMessage = new EventSendMessage(this.context);
            eventSendMessage.setExecutionTime(this.context.getActualTime());
            nextEvents.add(eventSendMessage);
        }

        return nextEvents;
    }


}
