package simmulation.ServerAEvents;

import communication.Acknowledge;
import server.ServerA;
import simmulation.Context;
import simmulation.Event;

import java.util.ArrayList;

/**
 * Created by jorge on 6/15/15.
 * Event that executes after server A receives an ack
 * from server B
 */
public class EventReceivedAcknowledge extends Event {

    private Acknowledge receivedAcknowledge;

    public EventReceivedAcknowledge(Context context, Acknowledge acknowledge)
    {
        super(context);
        this.receivedAcknowledge = acknowledge;
    }

    @Override
    public void execute() {
        super.execute();
        ServerA serverA = context.getServerA();
        //shift window
        serverA.shiftWindow(receivedAcknowledge);

        serverA.setLastAckReceived(receivedAcknowledge);
        context.deleteTimersBeforeFrameId(receivedAcknowledge.getFrameId());

        if( receivedAcknowledge.getFrameId() < serverA.getLastSentFrameId() )
        {
            //this set all the frames to resend
            serverA.prepareWindowToResend();
            context.deleteAllSendMessageEvents();
        }

    }

    @Override
    public ArrayList<Event> generateNextEvents() {
        ArrayList<Event> nextEvents = super.generateNextEvents();

        ServerA serverA = context.getServerA();
        if( !serverA.isSendProcessBusy() && serverA.canSendMoreMessageFromWindow() )
        {
            EventSendMessage eventSendMessage = new EventSendMessage(this.context);
            eventSendMessage.setExecutionTime(this.context.getActualTime());
            nextEvents.add(eventSendMessage);
        }

        return nextEvents;
    }
}
