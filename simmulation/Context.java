package simmulation;

import org.joda.time.DateTime;
import server.ServerA;
import server.ServerB;
import simmulation.ServerAEvents.EventSendMessage;
import simmulation.ServerAEvents.EventTimer;

import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Here comes all variables related to the state of the simulation
 */
public class Context {

    private boolean debugging = false;

    public static final int _TRANSFER_FRAME_TO_LINE_SECS = 1;
    public static final double _PROBABILITY_FRAME_WITH_ERROR = 0.1;
    public static final double _PROBABILITY_LOST_FRAME = 0.05;
    public static final double _PROBABILITY_LOST_ACK = 0.15;

    private Random normalRandomGen = new Random();
    private Random exponentialRandomGen = new Random();
    private Random customRandom = new Random();

    private ServerA serverA;
    private ServerB serverB;
    private DateTime actualTime;

    //variables for message generation random numbers
    private double msgGenMean = 3.5;
    private double msgGenVar = 1;

    //variables for message preparation/sent
    private double msgSentGenMean = 2;

    //variable for communicate to main process
    private Heap eventHeap;

    public Context(Heap eventHeap) {
        this.serverA = new ServerA();
        this.serverA.setTimerSeconds(7);
        this.serverB = new ServerB();
        this.actualTime = new DateTime();
        this.eventHeap = eventHeap;
    }

    public ServerA getServerA() {
        return serverA;
    }

    public void setServerA(ServerA serverA) {
        this.serverA = serverA;
    }

    public ServerB getServerB() {
        return serverB;
    }

    public void setServerB(ServerB serverB) {
        this.serverB = serverB;
    }

    public DateTime getActualTime() {
        return actualTime;
    }

    public void setActualTime(DateTime actualTime) {
        this.actualTime = actualTime;
    }

    /*
     *
     * Methods for generate random numbers for simulation
     *
     */

    /**
     * Arrival message time generator
     *
     * @return
     */
    public long generateCreateMessageTime() {
        long randNumber = (long) this.getNormalRandom(msgGenMean, msgGenVar, normalRandomGen);
        return randNumber;
    }

    /**
     * Exponential time for preparation of message, and time to put it on the line of
     * communication
     *
     * @return
     */
    public double generateSentFrameTime() {
        //seconds preparing frame
        double randNumber = this.getExponentialRandom(exponentialRandomGen, msgSentGenMean);
//        String temp = "" + randNumber;
//        String temp1 = temp.replace(".",",");
//        this.printLn("Generated only for prepare: " + temp1);
        randNumber = randNumber + (double) _TRANSFER_FRAME_TO_LINE_SECS;
        return randNumber;
    }

    public double generateCheckedFrameTime() {
        double randNumber = this.getCustomRandom(this.customRandom);
        return randNumber;
    }

    public double generateAcknowledgeToLineTime() {
        return 0.25;
    }

    /**
     * Decides if frame ready to send will contain errors
     */
    public boolean willTheFrameHaveErrors() {
        return Math.random() < _PROBABILITY_FRAME_WITH_ERROR;
    }

    /**
     * Decides if the frame will be lost
     */
    public boolean willTheFrameBeLost() {
        return Math.random() < _PROBABILITY_LOST_FRAME;
    }

    public boolean willTheACKBeLost() {
        return Math.random() < _PROBABILITY_LOST_ACK;
    }


    /**
     * Generate time for message receive in Server B
     */
    public long generateReceiveFrameTime() {
        return 1;
    }


    /**
     * Generates the time for Event in Server A who receives the ACK
     *
     * @param dateTime
     * @return
     */
    public DateTime generateReceiveACKDateTime(DateTime dateTime) {
        long eventReceiveACKMillis = 1000;
        DateTime newDateTime = dateTime.plus(eventReceiveACKMillis);
        return newDateTime;
    }

    /**
     * Normal distribution generator
     *
     * @param aMean
     * @param aVariance
     * @return
     */
    private double getNormalRandom(double aMean, double aVariance, Random fRandom) {
        return aMean + fRandom.nextGaussian() * aVariance;
    }


    /**
     * Exponential distribution
     *
     * @param r
     * @param p
     * @return
     */
    public double getExponentialRandom(Random r, double p) {
        return -(Math.log(r.nextDouble()) / p);
    }

    public double getCustomRandom(Random r) {
        double random = r.nextDouble();
        double x = Math.sqrt(5 * random + 4);
        return x;
    }

    public double convertSecsToMillis(double secsFuture) {
        return secsFuture * 1000.0;
    }

    public void printLn(String s) {
        System.out.print(s);
        System.out.println();
    }

    public void log(String from, String message) {
        if (debugging) {
            System.out.print(from + ": " + message);
            System.out.println();
        }
    }


    /**
     * Delete all alarms(timers) before
     */
    public void deleteTimersBeforeFrameId(long frameId) {
        ArrayList<Event> tempEvents = new ArrayList<>();

        boolean canCon = !this.eventHeap.isEmpty();
        while (canCon && !eventHeap.isEmpty() ) {
            Event event = (Event) eventHeap.removeMin();
            if( event instanceof EventTimer )
            {
                EventTimer timer = (EventTimer) event;
                if( timer.getFrameToResend().getId() > frameId )
                {
                    tempEvents.add(timer);//put the timer in list for add it to the queue again
                }else {
                    canCon = false;
                }

            }else{
                tempEvents.add(event);
            }
        }

        for(Event event: tempEvents)
        {
            eventHeap.insert(event);
        }

    }


    /**
     * When a timer is executed, an no ack is received, must delete all send messages
     * events for queue a new one
     */
    public void deleteAllSendMessageEvents() {
        ArrayList<Event> tempEvents = new ArrayList<>();

        while (!eventHeap.isEmpty() ) {
            Event event = (Event) eventHeap.removeMin();
            if( ! (event instanceof EventSendMessage) )
            {
                tempEvents.add(event);
            }
        }

        for(Event event: tempEvents)
        {
            eventHeap.insert(event);
        }

    }
}
