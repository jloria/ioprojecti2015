package simmulation;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.lang.Comparable;

/**
 * Created by jorge on 6/10/15.
 */
public class Event implements Comparable {

    protected Context context;
    protected DateTime executionTime;

    public Event(Context context)
    {
        this.context = context;
    }

    public void execute(){};

    public ArrayList<Event> generateNextEvents(){return new ArrayList<>();}

    @Override
    public int compareTo(Object o) {
        Event compareTo = (Event)o;
        return executionTime.compareTo(compareTo.getExecutionTime()) ;
    }

    public DateTime getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(DateTime executionTime) {
        this.executionTime = executionTime;
    }

    @Override
    public String toString() {
        return "Event " + getClass().getName() + "\n\t" +
                "Clock=" + executionTime
                ;
    }

}
