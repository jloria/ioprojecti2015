package simmulation.ServerBEvents;

import communication.Frame;
import org.joda.time.DateTime;
import simmulation.Context;
import simmulation.Event;

import java.util.ArrayList;

/**
 * Created by jorge on 6/12/15.
 */
public class EventReceiveFrame extends Event {

    Frame receivedFrame;

    public EventReceiveFrame(Context context, Frame frame)
    {
        super(context);
        this.receivedFrame = frame;
    }

    @Override
    public void execute() {
        super.execute();
        context.getServerB().addReceivedFrame(this.receivedFrame);
    }

    @Override
    public ArrayList<Event> generateNextEvents() {
        ArrayList<Event> nextEvents = super.generateNextEvents();

        if( !context.getServerB().isCheckFramesProcessBusy() )
        {
            //create event check frame
            context.getServerB().setCheckFramesProcessBusy(true);
            EventCheckedFrame eventCheckedFrame = new EventCheckedFrame(context);
            double checkedFrameSecs = context.generateCheckedFrameTime();
            long checkedFrameMillis = (long)context.convertSecsToMillis(checkedFrameSecs);
            DateTime eventCheckedFrameTime = context.getActualTime().plus(checkedFrameMillis);
            eventCheckedFrame.setExecutionTime(eventCheckedFrameTime);
            nextEvents.add(eventCheckedFrame);
        }

        return nextEvents;
    }
}
