package simmulation.ServerBEvents;

import communication.Acknowledge;
import communication.Frame;
import org.joda.time.DateTime;
import server.ServerB;
import simmulation.Context;
import simmulation.Event;
import simmulation.ServerAEvents.EventFrameSent;

import java.util.ArrayList;

/**
 * Created by jorge on 6/15/15.
 * This event fires up when the process of check the incoming frame from Server A
 * is ready
 */
public class EventCheckedFrame extends Event {

    //indicates if the ack will be send
    private boolean sendAcknowledge;
    private Acknowledge acknowledgeToSend;

    public EventCheckedFrame(Context context){
        super(context);
        sendAcknowledge = false;
    }

    @Override
    public void execute() {
        super.execute();
        ServerB serverB = context.getServerB();
        Frame frameToCheck = serverB.obtainFrameToCheck();
        long frameWaitingId = serverB.getFrameIdWaitingFor();
        sendAcknowledge = frameToCheck != null;
        if(sendAcknowledge) {
            if (frameWaitingId == frameToCheck.getId()
                    && !frameToCheck.hasError()) {
                serverB.incrementFrameWaitId();
                acknowledgeToSend = new Acknowledge(serverB.getFrameIdWaitingFor());
            } else if ( serverB.getFrameIdWaitingFor() != frameToCheck.getId() ) {
                acknowledgeToSend = new Acknowledge(frameWaitingId);
            }else{
                sendAcknowledge = false;
            }
        }
        //If ACK is not marked to be sent, then the process is ready
        //to check another frame
        serverB.setCheckFramesProcessBusy(sendAcknowledge);

    }

    @Override
    public ArrayList<Event> generateNextEvents() {
        ArrayList<Event> nextEvents = super.generateNextEvents();
        if(sendAcknowledge) {
            EventSentAcknowledge eventSentAcknowledge = new EventSentAcknowledge(context, acknowledgeToSend);
            double timeToSentSecs = context.generateAcknowledgeToLineTime();
            long timeToSentMillis = (long) context.convertSecsToMillis(timeToSentSecs);
            DateTime eventSentTime = context.getActualTime().plus(timeToSentMillis);
            eventSentAcknowledge.setExecutionTime(eventSentTime);
            nextEvents.add(eventSentAcknowledge);
        }
        return nextEvents;
    }

    @Override
    public String toString()
    {
        StringBuilder s =  new StringBuilder(super.toString());
        if(sendAcknowledge)
            s.append("\n\tEventMessage: ACK sending:" + acknowledgeToSend.getFrameId());

        return s.toString();
    }

}
