package simmulation.ServerBEvents;

import communication.Acknowledge;
import simmulation.Context;
import simmulation.Event;
import simmulation.ServerAEvents.EventReceivedAcknowledge;

import java.util.ArrayList;

/**
 * Created by jorge on 6/15/15.
 * Event that happens after the ack finish the send process
 */
public class EventSentAcknowledge extends Event {

    private Acknowledge ack;

    public EventSentAcknowledge(Context context, Acknowledge ack)
    {
        super(context);
        this.ack = ack;
    }

    @Override
    public void execute() {
        super.execute();
        this.context.getServerB().setCheckFramesProcessBusy(false);
    }

    @Override
    public ArrayList<Event> generateNextEvents() {
        ArrayList<Event> nextEvents = super.generateNextEvents();

        if(!context.willTheACKBeLost())
        {
            EventReceivedAcknowledge eventReceivedAcknowledge = new EventReceivedAcknowledge(context,this.ack);
            eventReceivedAcknowledge.setExecutionTime(context.generateReceiveACKDateTime(context.getActualTime()));
            nextEvents.add(eventReceivedAcknowledge);
        }

        return nextEvents;
    }
}
