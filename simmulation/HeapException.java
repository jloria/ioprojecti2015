package simmulation;

/**
 * Created by jorge on 6/10/15.
 */
public class HeapException extends RuntimeException {
    public HeapException(){};
    public HeapException(String msg){super(msg);}
}