package communication;

/**
 * Created by jorge on 6/10/15.
 */
public class ListMessagesField {
    private Frame frame;
    private Message message;

    private boolean sent;

    public ListMessagesField(Message message)
    {
        this.message = message;
        this.sent = false;
    }

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public Message getMessage() {
        return message;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

}
