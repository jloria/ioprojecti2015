package communication;

import java.util.ArrayList;

/**
 * Data structure for store
 * messages to send from server A to server B
 * This manage window and which is the next message to send.
 */
public class ListMessages {

    private ArrayList<ListMessagesField> list;
    private static final int _WINDOW_SIZE = 8;

    public ListMessages() {
        this.list = new ArrayList<>();
    }

    public void addMessage(Message message)
    {
        this.list.add(new ListMessagesField(message));
    }

    public boolean windowIsFull() {

        boolean isFull = this.list.size() >= this._WINDOW_SIZE;
        return isFull;
    }


    /**
     * Search in the window for a message
     * @return
     */
    public ListMessagesField getNextMessageToSend()
    {
        ListMessagesField messageToSend = null;
        boolean goNext = true;
        int listSize = this.list.size();
        for(int i = 0; i < listSize && i < this._WINDOW_SIZE && goNext == true ; ++i)
        {
            messageToSend  = this.list.get(i);
            goNext = messageToSend.isSent();
        }
        return messageToSend;
    }

    /**
     * Search in window for messages to send
     * @return
     */
    public boolean windowHasMessageToSend() {

        boolean hasIt = false;
        int listSize = this.list.size();
        for(int i = 0; i < listSize && i < _WINDOW_SIZE && !hasIt; ++i)
        {
            ListMessagesField messagesField = list.get(i);
            hasIt = !messagesField.isSent();
        }
        return hasIt;
    }

    public void shiftWindowToFrameId(long frameId) {
        boolean cont = true;
        while (cont && list.size() > 0 )
        {
            cont = false;
            ListMessagesField messagesField = list.get(0);
            Frame frame = messagesField.getFrame();
            if(frame != null)
            {
                cont = frame.getId() < frameId;
                if(cont)
                    list.remove(0);
            }
        }
    }

    /**
     * shift messages windows to resend
     * this is, mark all ListMessagesFields as not sent
     */
    public void shiftWindowToResend()
    {
        int size = list.size();
        for(int i = 0; i < size && i < _WINDOW_SIZE; ++i)
        {
            list.get(i).setSent(false);
        }
    }

    public int size() {
        return list.size();
    }

    public String printList(int maxSize) {
        StringBuilder builder = new StringBuilder();

        int listSize = list.size();

        builder.append("   Message List:");
        for(int i = 0; i < listSize && i < maxSize; ++i)
        {
            char wn = (i<_WINDOW_SIZE)?'W':'L';
            Frame frame = list.get(i).getFrame();
            String m = ( frame != null)?"Frame id:" + frame.getId() :"Message";

            builder.append( "\n\t" + (i+1) + "-" + wn + " " + m );
        }

        return builder.toString();
    }

}
