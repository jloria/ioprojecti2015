package communication;

/**
 * Created by jorge on 6/15/15.
 */
public class Acknowledge {

    private long frameId;

    public Acknowledge(long frameId)
    {
        this.frameId = frameId;
    }

    public long getFrameId() {
        return frameId;
    }

    public void setFrameId(long frameId) {
        this.frameId = frameId;
    }
}
