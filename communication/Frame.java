package communication;

/**
 * Created by jorge on 6/10/15.
 */
public class Frame {

    private long id;
    private Message message;
    private boolean sent;
    private boolean hasError;

    public Frame(long id, Message message)
    {
        this.id = id;
        this.message = message;
        this.sent = false;
    }

    public Message getMessage() {
        return message;
    }

    public long getId() {
        return id;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean hasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }
}
